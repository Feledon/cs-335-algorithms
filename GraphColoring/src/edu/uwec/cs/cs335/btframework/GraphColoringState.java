package edu.uwec.cs.cs335.btframework;

import java.util.List;


public class GraphColoringState implements State, Comparable<GraphColoringState>{
	private boolean[][] graph;
	private List<ColorNode> colors;
	private int[] board;
	private int nextRow;
	private int nextCol;

	public GraphColoringState(boolean[][] graph, List<ColorNode> colors){
		this.graph = graph;
		this.colors = colors;
		this.nextRow = 0;
		this.nextCol = 0;
		this.board = new int[graph.length];
		for (int i=0; i<graph.length; i++) {
			this.board[i] = -1;
		}
	}
	
	public GraphColoringState(GraphColoringState gcs) {
		// make a deep copy
		this.colors = gcs.colors;
		this.graph = gcs.graph;
		this.board = new int[graph.length];
		for (int j = 0; j < gcs.graph.length; j++){
			this.board[j] = gcs.board[j];
		}
		this.nextRow = gcs.nextRow;
		this.nextCol = gcs.nextCol;
	}

	public boolean hasMoreChildren() {
		return (nextRow < colors.size());
	}

	public State nextChild() {
		GraphColoringState child = new GraphColoringState(this);
		// Modify the child's board
		child.board[this.nextCol] = this.nextRow;

		// Set myself up to produce more children
		this.nextRow++;

		// Setup the child so it can produce the correct children
		child.nextCol++;
		child.nextRow = 0;

		return child;
	}

	public boolean isFeasible() {
		int j = 0;
		boolean feasible = true;
		int num = this.nextCol - 1;

		while(j < num && feasible) {
			if(graph[num][j] && board[num] == board[j]) {//IF this is a connected node, and the the values are equal, no.
				feasible = false;
			}
			j++;
		}
		return feasible;
	}

	// Assuming it is feasible
	public boolean isSolved() {
		return (nextCol == graph.length);
	}

	public int getBound() {
		int lowest = Integer.MAX_VALUE;
		for(int i = 0; i < colors.size(); i++){
			if(colors.get(i).getCost() < lowest){
				lowest = colors.get(i).getCost();
			}
		}		
		int total = 0;		
		for(int j = 0; j < nextCol; j++){
//						System.out.print("j: " + j + " prev total: " + total);
			total = total + colors.get(board[j]).getCost();
//						System.out.println(" added weight: " + colors.get(board[j]).getCost() + " new weight: " + total);
		}
//				System.out.println("total: " + total);
		total += lowest * (graph.length - (nextCol - 1));
//				System.out.println("next total: " + total);
		return total;
	}

	public String toString() {
		String result = "<";
		for (int i=0; i<graph.length; i++) {
			result += colors.get(board[i]).getColor() + " ";
		}
		result += ">";
		return result;
	}

	public int compareTo(GraphColoringState arg0) {
		int ret = 0;
		if(this.board==arg0.board){
			ret = 1;
		}
		return ret;
	}
}
