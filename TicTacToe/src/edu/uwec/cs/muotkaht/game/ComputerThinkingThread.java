package edu.uwec.cs.muotkaht.game;

public class ComputerThinkingThread implements Runnable{

	private GamePlayer computer;
	private boolean moveOn;
	
	public ComputerThinkingThread(GamePlayer player) {
		this.computer = player;
		this.moveOn = true;
	}
	
	public void cancel() {
		this.moveOn = false;
	}
	
	@Override
	public void run() {
		TwoPlayerGameBoard nextBoard = this.computer.getMiniMax().generateNextMove(this.computer.getBoard());
		
		if (this.moveOn) {
			computer.computerDone(nextBoard);
		}
	}

}
