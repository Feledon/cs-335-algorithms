package edu.uwec.cs.gehrkeb.path;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Main class for finding a path using A star algorithm.
 * @author gehrkeb
 *
 */
public class PathFinder {

	private TerrainMap tm;
	private TerrainPanel tp;

	public PathFinder(TerrainMap tm, TerrainPanel tp) {
		this.tm = tm;
		this.tp = tp;
	}

	// The A* algorithm
	public List<Point> findPath(Point start, Point end) {
		List<Point> returnPoints = new ArrayList<Point>();
		List<Point> visitedPoints = new ArrayList<Point>();
		visitedPoints.add(start);

		PathState path = new PathState(start, end, tm);

		PriorityQueue<PathState> statesToProcess = new PriorityQueue<PathState>();  // Best-first
		statesToProcess.add(path);
		boolean isFound = false;
		while (!statesToProcess.isEmpty() && !isFound) {			
			PathState currentState = statesToProcess.poll();  // Best-first
			if(currentState.isGoal()){
				returnPoints = currentState.getPath();
				returnPoints.add(currentState.currentPoint);			
				isFound = true;			
				statesToProcess.clear();
			}else{
				while(currentState.hasMoreChildren()){
					PathState child = currentState.getNextChild();

					if(!visitedPoints.contains(child.currentPoint)){
						statesToProcess.offer(child);//	
						visitedPoints.add(child.currentPoint);					
						
						tp.addAstarState(child);
						tp.repaint();
						if(child.currentPoint.equals(end)){
							break;
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException ie) {
							System.out.println(ie.getMessage());
						}
					}
				}
			}
		}
		return returnPoints;	
	}
}

