package edu.uwec.cs.gehrkeb.assignment2;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MergeHullOld implements ConvexHullFinder {

	@Override
	public List<Point2D> computeHull(List<Point2D> points) {
		System.out.println("MERGE HULL");
		
		// sort the points left to right
		Collections.sort(points, new Comparator<Point2D>() {
			@Override
			public int compare(Point2D p1, Point2D p2) {
				if(p1.getX() < p2.getX()) {
					return -1;
				} else if(p1.getX() > p2.getX()) {
					return 1;
				} 
				return 0;
			}
        });
	
		List<Point2D> completeHull = recursiveMergeHull(points);
		System.out.println(" Final POINTS: " + points);
		return completeHull;
	}
	
	private List<Point2D> recursiveMergeHull(List<Point2D> points) {
        // Base: One or two points, must be a hull itself
		if(points.size() <= 3) {
			return points;
		} else {
			List<Point2D> hull = new ArrayList<Point2D>();
			List<Point2D> remove = new ArrayList<Point2D>();
			List<Point2D> leftHalf = points.subList(0, points.size() / 2);
			List<Point2D> rightHalf = points.subList(points.size() / 2, points.size());
			System.out.println("LEFT HALF: " + leftHalf);
			System.out.println("RIGHT Half: " + rightHalf);
			
			// Recursively Computes the hull of each subset
			List<Point2D> leftHull = recursiveMergeHull(leftHalf);
	        List<Point2D> rightHull = recursiveMergeHull(rightHalf);
	        System.out.println("LEFT HULL: " + leftHull.toString());
	        System.out.println("RIGHT HULL: " + rightHull.toString());
	        
	        // Find the rightmost point in the left hull
	        Point2D rightMostInLeft = getRightMost(leftHull);
	        System.out.println("RIGHT MOST: " + rightMostInLeft);
	        
	        // Find the leftmost point in the right hull
	        Point2D leftMostInRight = getLeftMost(rightHull);
	        System.out.println("LEFT MOST: " + leftMostInRight);
	        
	        // Connect right and left most to form starting tangent
//	        Line2D startingTangent = new Line2D.Double(rightMostInLeft, leftMostInRight);
//	        boolean found = false;
	        boolean foundLeft = false;
	        boolean foundRight = false;
	        
	        /**
A = rightmost point of left hull
B = leftmost point of right hull
	         */
	        
	        // Walk until starting tangent is now the lower tangent line
	        while(!foundLeft && !foundRight){//While (T = AB is not the lower tangent to both left and right hulls) {
	        	Point2D temp = new Point2D.Double();
	        	
	        	double oldSlope = 0.0;
	        	double newSlope = 0.0;
	        	
	        	while(!foundLeft){//While (T not lower tangent to left hull) {
	        		int i = leftHull.indexOf(rightMostInLeft);	     
	        		// A = A � 1 (assumption is that hulls are defined in ccw order)
	        		if(i > 0){
	        			temp = leftHull.get(i - 1);
	        			newSlope = getSlope(temp, leftMostInRight);
	        			oldSlope = getSlope(rightMostInLeft,leftMostInRight);
	        		}
	        		if(i > 0 && newSlope < oldSlope){
	        			rightMostInLeft = leftHull.get(i - 1);
	        			leftHull.remove(rightMostInLeft);
	        			foundRight = false;
	        		}else{
	        			foundLeft = true;
	        		}
	        			
	        	}
	        	while(!foundRight){//While (T not lower tangent to right hull) {
	        		//(rightHull.get(rightHull.indexOf(leftMostInRight) + 1)) != null && 
	        		int i = rightHull.indexOf(leftMostInRight);
	        		if((i + 2) <= (rightHull.size())){//0 based index, size starts at 1, so compensate
	        			temp = rightHull.get(i + 1);
	        			newSlope = getSlope(temp, rightMostInLeft);
	        			oldSlope = getSlope(leftMostInRight,rightMostInLeft);
	        		}
	        		if((i + 2) < (rightHull.size()) && newSlope > oldSlope){
	        			// B = B + 1 (assumption is that hulls are defined in ccw order)
	        			leftMostInRight = rightHull.get(i + 2);
	        			rightHull.remove(leftMostInRight);
	        			foundLeft = false;
	        		}else{
	        			foundRight = true;
	        		}      			
	        		
	        	}	        	
	        }
	        
	        if(rightHull.size() > 2){
	        	Line2D line = new Line2D.Double(rightMostInLeft, leftMostInRight);
	        }
	        
	        
//	        hull.add(rightMostInLeft);
//	        hull.add(leftMostInRight);
	        hull.addAll(rightHull);
	        hull.addAll(leftHull);
	        return hull;
		}
		
	}
	
	private double getSlope(Point2D p, Point2D q){
	  return (p.getY() - q.getY())/(p.getX()/q.getX());
	}
	
	private Point2D getRightMost(List<Point2D> leftHull) {
		if(!leftHull.isEmpty()) {
			Point2D rightMost = Collections.max(leftHull, new Comparator<Point2D>() {
				@Override
				public int compare(Point2D p1, Point2D p2) {
					if(p1.getX() < p2.getX()) {
						return -1;
					} else if(p1.getX() > p2.getX()) {
						return 1;
					} 
					return 0;
				}
	        });
			return rightMost;
		} else {
			return null;
		}
	//	Point2D rightMost = Collections.min(leftHull, null);	

	}
	
	private Point2D getLeftMost(List<Point2D> rightHull) {
		if(!rightHull.isEmpty()) {
			Point2D leftMost = Collections.min(rightHull, new Comparator<Point2D>() {
				@Override
				public int compare(Point2D p1, Point2D p2) {
					if(p1.getX() < p2.getX()) {
						return -1;
					} else if(p1.getX() > p2.getX()) {
						return 1;
					} 
					return 0;
				}
	        });
			return leftMost;
		} else {
			return null;
		}
	}
	
}
