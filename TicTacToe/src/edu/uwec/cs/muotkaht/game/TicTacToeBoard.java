package edu.uwec.cs.muotkaht.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import javax.swing.JOptionPane;

public class TicTacToeBoard implements TwoPlayerGameBoard {

	private static final int BOARD_SIZE = 9;

	private char[] board;
	private int nextOpenPosition;
	private boolean isComputerMove;

	public TicTacToeBoard() {
		this.board = new char[BOARD_SIZE];
		for(int i = 0; i < BOARD_SIZE; i++) {
			this.board[i] = '_';
		}

		this.nextOpenPosition = 0;
		this.isComputerMove = false;
	}

	// Test constructor
	public TicTacToeBoard(char[] board, int nextOpenPosition, boolean isComputerMove) {
		this.board = board;
		this.nextOpenPosition = nextOpenPosition;
		this.isComputerMove = isComputerMove;
	}

	// Copy constructor
	public TicTacToeBoard(TicTacToeBoard original) {
		this.board = new char[BOARD_SIZE];

		for(int i = 0; i < BOARD_SIZE; i++) {
			this.board[i] = original.board[i];
		}

		this.nextOpenPosition = original.nextOpenPosition;
		this.isComputerMove = original.isComputerMove;
	}

	// Checks if state has any unchecked children
	@Override
	public boolean hasMoreChildren() {
		return (nextOpenPosition < BOARD_SIZE);
	}

	@Override
	public TwoPlayerGameBoard nextChild() {
		TicTacToeBoard child = new TicTacToeBoard(this);

		// Place an X for computer move
		child.board[nextOpenPosition] = (isComputerMove) ? 'X' : 'O';

		// Sets up the next child
		this.nextOpenPosition++;

		while(this.nextOpenPosition < BOARD_SIZE && this.board[this.nextOpenPosition] != '_') {
			this.nextOpenPosition++;
		}

		child.isComputerMove = !child.isComputerMove;
		child.nextOpenPosition = 0;

		while(child.nextOpenPosition < BOARD_SIZE && child.board[child.nextOpenPosition] != '_') {
			child.nextOpenPosition++;
		}

		return child;
	}

	// Looks at board's current state and determines the outcome
	@Override
	public double staticEvaluation() {
		boolean hasWon = hasColWin() || hasRowWin() || hasDiagonalWin();

		if(isComputerMove && hasWon) { // computer lost
			return -1.0;

		} else if(!isComputerMove && hasWon) { // computer won
			return 1.0;

		} else { // draw
			return 0.0;
		}

	}

	// Checks if the board has a col win
	private boolean hasColWin() {
		boolean hasWon = false;
		int rowLength = (int) Math.sqrt(BOARD_SIZE);
		char top, middle, bottom;

		for(int i = 0; i < rowLength; i++) {
			top = this.board[i];
			middle = this.board[i + rowLength];
			bottom = this.board[i + 2 * rowLength];

			if(top != '_' && middle == top && bottom == top) {
				hasWon = true;
			}
		}
		return hasWon;
	}

	// Checks if the board has a row win
	private boolean hasRowWin() {
		boolean hasWon = false;
		int rowLength = (int) Math.sqrt(BOARD_SIZE);
		char left, middle, right;

		for(int i = 0; i < rowLength; i++) {
			left = this.board[i * rowLength];
			middle = this.board[i * rowLength + 1];
			right= this.board[i * rowLength + 2];

			if(left != '_' && middle == left && right == left) {
				hasWon = true;
			}
		}


		return hasWon;
	}

	// Checks if board has diagonal win
	private boolean hasDiagonalWin() {
		boolean hasWon = false;

		char upperLeft = this.board[0];
		char upperRight = this.board[2];
		char center = this.board[4];
		char lowerLeft = this.board[6];
		char lowerRight = this.board[8];

		if((upperLeft != '_' && upperLeft == center && upperLeft == lowerRight) ||
				(upperRight != '_' && upperRight == center && upperRight == lowerLeft)) {
			hasWon = true;
		}

		return hasWon;
	}

	// Draws the board and the X's and O's
	@Override
	public void draw(Graphics g) {

		g.setColor(Color.GREEN);
		g.drawLine(100, 0, 100, 300);
		g.drawLine(200, 0, 200, 300);
		g.drawLine(0, 100, 300, 100);
		g.drawLine(0, 200, 300, 200);

		for(int i = 0; i < board.length; i++) {
			if(board[i] == 'X') { // draw X
				g.setColor(Color.MAGENTA);
				if (i == 0) {
					g.drawLine(0, 0, 100, 100);
					g.drawLine(0, 100, 100, 0);
				}
				if (i == 1) {
					g.drawLine(100, 0, 200, 100);
					g.drawLine(100, 100, 200, 0);
				}
				if (i == 2) {
					g.drawLine(200, 0, 300, 100);
					g.drawLine(200, 100, 300, 0);
				}
				if (i == 3) {
					g.drawLine(0, 100, 100, 200);
					g.drawLine(0, 200, 100, 100);
				}
				if (i == 4) {
					g.drawLine(100, 100, 200, 200);
					g.drawLine(100, 200, 200, 100);
				}
				if (i == 5) {
					g.drawLine(200, 200, 300, 100);
					g.drawLine(200, 100, 300, 200);
				}
				if (i == 6) {
					g.drawLine(0, 300, 100, 200);
					g.drawLine(0, 200, 100, 300);
				}
				if (i == 7) {
					g.drawLine(100, 300, 200, 200);
					g.drawLine(100, 200, 200, 300);
				}
				if (i == 8) {
					g.drawLine(200, 300, 300, 200);
					g.drawLine(200, 200, 300, 300);
				}
			} else if(board[i] == 'O') { // draw O
				g.setColor(Color.CYAN);
				g.drawOval(i % 3 * 100 + 5,  (i/3) * 100 + 5, 90, 90);
			}
		}

	}

	// Checks if computer is winner
	@Override
	public boolean isComputerWinner() {
		return (!this.isComputerMove && (hasColWin() || hasRowWin() || hasDiagonalWin()));
	}

	// Checks if is a draw
	@Override
	public boolean isDraw() {
		boolean isGameFinished = true;
		for (int i = 0; i < this.board.length; i++) {
			if (this.board[i] == '_') {
				isGameFinished = false;
			}
		}
		return (isGameFinished && !hasColWin() && !hasRowWin() && !hasDiagonalWin());
	}

	// Checks if user is winner
	@Override
	public boolean isUserWinner() {
		return (this.isComputerMove && (hasColWin() || hasRowWin() || hasDiagonalWin()));	
	}

	// Places the move on the board from the mouse location 
	@Override
	public void placeUserMove(Point2D mouseLocation) throws Exception {
		int row = (int) mouseLocation.getY() / 100;
		int col = (int) mouseLocation.getX() / 100;

		int boardIndex = row * 3 + col;

		//throw an exception if click outside of bounds
		if (row > 2 || col > 2 || row < 0 || col < 0 || board[boardIndex] == 'O' || board[boardIndex] == 'X') {
			JOptionPane.showMessageDialog(null, "Click within the board");
			throw new Exception();
		}

		this.board[boardIndex] = 'O';

		this.isComputerMove= !this.isComputerMove;

		this.nextOpenPosition = 0;
		while (this.nextOpenPosition < BOARD_SIZE && this.board[this.nextOpenPosition] != '_') {
			this.nextOpenPosition++;
		} 

	}

	// prints out board
	public String toString() {
		String boardString = "";
		int rowLength = (int)Math.sqrt(BOARD_SIZE);

		for (int i = 0; i < BOARD_SIZE; i++) {
			boardString += board[i] + "\t";

			if ((i + 1) % rowLength == 0) {
				boardString += "\n";
			}
		}
		return boardString;
	}
}
