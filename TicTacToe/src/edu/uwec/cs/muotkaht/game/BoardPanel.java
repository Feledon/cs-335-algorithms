package edu.uwec.cs.muotkaht.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class BoardPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private TwoPlayerGameBoard board = null;
	
    @Override 
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	if (this.board != null) {
			
			Font messageFont = new Font("Calibri", Font.BOLD, 30);
			
			// Tie -- no winner
			if (this.board.isDraw()) {
				g.setColor(Color.WHITE);
				g.setFont(messageFont);
				g.drawString("Draw!", 75, 350);
				this.board.draw(g);
			
			// Computer won
			} else if (this.board.isComputerWinner()) {
				g.setColor(Color.white);
				g.setFont(messageFont);
				g.drawString("Loser.", 75, 350);
				this.board.draw(g);
				
			// User won
			} else if (this.board.isUserWinner()) {
				g.setColor(Color.WHITE);
				g.setFont(messageFont);
				g.drawString("Winner!", 75, 350);
				this.board.draw(g);
				
			// in progress
			} else {
				g.setColor(Color.WHITE);
				g.setFont(messageFont);
				g.drawString("In progess.", 75, 350);
				this.board.draw(g);		
			}
		}
    	
    }
    
    public void setBoard(TwoPlayerGameBoard newBoard) {
    	this.board = newBoard;
    	repaint();
    }
}
