package edu.uwec.cs.gehrkeb.assignment2;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MergeHull implements ConvexHullFinder {
	public List<Point2D> computeHull(List<Point2D> points) {
		Collections.sort(points, new Comparator<Point2D>() {
			@Override
			public int compare(Point2D p1, Point2D p2) {
				int result;
				if (p1.getX() < p2.getX())
					result = -1;
				else if (p1.getX() > p2.getX())
					result = 1;
				else{
					result = 0;
				}
				return result;
			}
		});
		
		return recursiveMergeHull(points);
	}

	/**
	 * 
	 * @param points
	 * @return list of outer convex hull points
	 */
	private List<Point2D> recursiveMergeHull(List<Point2D> points) {
		// List of hull points
		List<Point2D> hullPoints = new ArrayList<Point2D>();

		// Base Case
		if (points.size() <= 1) {
			hullPoints = points;
		} else {// Recursive Case

			// Divide list of points
			List<Point2D> left = points.subList(0, points.size() / 2);
			List<Point2D> right = points.subList(points.size() / 2,	points.size());

			// Get left and right hulls, respectively
			List<Point2D> leftHull = recursiveMergeHull(left);
			List<Point2D> rightHull = recursiveMergeHull(right);

			// Merge hulls
			Point2D rightMost = getRightPoint(leftHull);
			Point2D leftMost = getLeftPoint(rightHull);
			Line2D upperTangent = findTang(new Line2D.Double(rightMost, leftMost), leftHull, rightHull);
			Line2D lowerTangent = findTang(new Line2D.Double(leftMost, rightMost), rightHull, leftHull);

			// Combine leftHull and RightHull
			int index = leftHull.indexOf(upperTangent.getP1());
			while (index != leftHull.indexOf(lowerTangent.getP2())) {
				hullPoints.add(leftHull.get(index));
				index++;
				if (index == leftHull.size())
					index = 0;
			}
			hullPoints.add(leftHull.get(index));

			index = rightHull.indexOf(lowerTangent.getP1());
			while (index != rightHull.indexOf(upperTangent.getP2())) {
				hullPoints.add(rightHull.get(index));
				index++;
				if (index == rightHull.size())
					index = 0;
			}
			hullPoints.add(rightHull.get(index));
		}
		return hullPoints;
	}
	
	private Point2D getRightPoint(List<Point2D> hullPoints) {
		double maxX = hullPoints.get(0).getX();
		Point2D rightmost = hullPoints.get(0);
		for (int i = 1; i < hullPoints.size(); i++) {
			double temp = hullPoints.get(i).getX();
			
			if((hullPoints.get(i).getX() == rightmost.getX()) && (hullPoints.get(i).getY() == rightmost.getY())){
//Skip duplicates
			}else if (temp > maxX) {
				maxX = temp;
				rightmost = hullPoints.get(i);
			}
		}
		return rightmost;
	}

	private Point2D getLeftPoint(List<Point2D> hullPoints) {
		double minX = hullPoints.get(0).getX();
		Point2D leftmost = hullPoints.get(0);
		for (int i = 1; i < hullPoints.size(); i++) {
			double temp = hullPoints.get(i).getX();
			if((hullPoints.get(i).getX() == leftmost.getX()) && (hullPoints.get(i).getY() == leftmost.getY())){
//Skip duplicates
			}else if (temp < minX) {
				minX = temp;
				leftmost = hullPoints.get(i);
			}
		}
		return leftmost;
	}

	private Line2D findTang(Line2D ab, List<Point2D> leftHull, List<Point2D> rightHull) {
		Point2D a = ab.getP1();
		Point2D b = ab.getP2();
		int i;

		while (notTangLeft(a, b, leftHull, rightHull)|| notTangRight(a, b, leftHull, rightHull)) {
			while (notTangLeft(a, b, leftHull, rightHull)) {
				i = leftHull.indexOf(a) + 1;
				if (i == leftHull.size()) {
					i = 0;
				}
				a = leftHull.get(i);
			}
			while (notTangRight(a, b, leftHull, rightHull)) {
				i = rightHull.indexOf(b) - 1;
				if (i == -1) {
					i = rightHull.size() - 1;
				}
				b = rightHull.get(i);
			}
		}
		return new Line2D.Double(a, b);
	}

	private boolean notTangLeft(Point2D a, Point2D b, List<Point2D> leftHull, List<Point2D> rightHull) {
		boolean notFoundTangent = false;
		Line2D tang = new Line2D.Double(b, a);
		int i = leftHull.indexOf(a) + 1;

		if (i == leftHull.size())
			i = 0;
		Point2D nextPoint = leftHull.get(i);

		if (tang.relativeCCW(nextPoint) == -1) {
			notFoundTangent = true;
		}
		return notFoundTangent;
	}

	private boolean notTangRight(Point2D a, Point2D b, List<Point2D> leftHull, List<Point2D> rightHull) {
		boolean notFoundTangent = false;
		Line2D tang = new Line2D.Double(a, b);
		int i = rightHull.indexOf(b) - 1;

		if (i == -1)
			i = rightHull.size() - 1;
		Point2D nextPoint = rightHull.get(i);
		if (tang.relativeCCW(nextPoint) == 1) {
			notFoundTangent = true;
		}
		return notFoundTangent;
	}
}