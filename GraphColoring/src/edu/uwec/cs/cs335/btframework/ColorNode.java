package edu.uwec.cs.cs335.btframework;

public class ColorNode {
	private int cost;
	public int getCost() {
		return cost;
	}
	public String getColor() {
		return color;
	}
	private String color;

	public ColorNode(int cost, String color){
		this.color = color;
		this.cost = cost;
	}
}
