package edu.uwec.cs.stevende.rsa;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;

public class BigIntegerRSA {

	static BigInteger p = BigInteger.ZERO;
	static BigInteger q = BigInteger.ZERO;
	static BigInteger d = BigInteger.ZERO;
	static BigInteger e = BigInteger.ZERO;
	static BigInteger pq = BigInteger.ZERO;
	static BigInteger phiPQ = BigInteger.ZERO; 	
	static String plainTextMessage = "TEst";
	static BigInteger[] cipherMessage = new BigInteger[plainTextMessage.length()];

	public static void encrypt(){
		// Encyrpt with:  message.modPow(e, n);
		for (int i=0; i<plainTextMessage.length(); i++) {
			BigInteger m = new BigInteger(((int)plainTextMessage.charAt(i)) + "");
			BigInteger c = m.modPow(e, pq);
			cipherMessage[i] = c;
		}

		// Write out the message to the screen
		//		System.out.println("The cipher message is:");
		//		for (int i=0; i<cipherMessage.length; i++) {
		//			System.out.println(cipherMessage[i]);
		//		}
	}

	public static void decrypt(){
		// decrypt with: message.modPow(d, n);
		String decodedMessage = "";
		for (int i=0; i<cipherMessage.length; i++) {
			BigInteger c = cipherMessage[i];
			BigInteger m = c.modPow(d, pq);
			decodedMessage += (char)(m.intValue());
		}

		//		System.out.println(decodedMessage);
	}


	public static void generateE(){
		// Find E
		e = new BigInteger("2");
		while (phiPQ.gcd(e).compareTo(BigInteger.ONE) > 0) {
			e = e.add(BigInteger.ONE);
		}
	}

	public static void generateD(){
		// Find D
		d = e.modInverse(phiPQ);
	}

	public static void breakIt(){
		//We have PG and E
		BigInteger i = BigInteger.valueOf(3);
		BigInteger lilP = BigInteger.ZERO;
		BigInteger lilQ = BigInteger.ZERO;

		while(i.compareTo(pq) < 0){
			if(pq.mod(i) == BigInteger.ZERO){
				lilP = i;
				lilQ = pq.divide(lilP);
				//				System.out.println("Possible P/q: " + lilP + "/" + lilQ);
				i = pq;
			}else{
				i = i.add(BigInteger.valueOf(2));
			}			
		}

		//Now have PQ, p, q, and E.
		phiPQ = (lilP.subtract(BigInteger.ONE)).multiply(lilQ.subtract(BigInteger.ONE));
		d = e.modInverse(phiPQ);
	}

	public static void main(String[] args) throws IOException {
		// Here is an all BigInteger implementation of RSA where it does all the work import java.math.BigInteger;
		int runTimes = 20;
		String[] times = new String[runTimes];
		int bitlen = 14;
		for(int i = 0; i < runTimes; i++){
			// Find P and Q
			bitlen ++;
			p = BigInteger.ZERO;
			q = BigInteger.ZERO;
			
			SecureRandom r = new SecureRandom();
			while (p.equals(q)) {
				p = new BigInteger(bitlen, 100, r);
				q = new BigInteger(bitlen, 100, r);
			}

			// Find PQ and phiPQ
			pq = p.multiply(q);
			phiPQ = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
			generateE();
			generateD();

			// Print out some information
			//			System.out.println("p: " + p);
			//			System.out.println("q: " + q);
			//			System.out.println("pq: " + pq);
			//			System.out.println("e: " + e);
			//			System.out.println("d: " + d);

			encrypt();

			long start = System.currentTimeMillis() ;
			breakIt();	
			long stop = System.currentTimeMillis();
			times[i] = stop - start + "	" + p.bitLength();
			System.out.println(p.bitLength() + " " + ((stop - start)));

			decrypt();			
		}
		write(times);
	}

	public static void write(String args[]) throws IOException {
		String fileName = "C:\\Users\\gehrkeb\\Desktop\\test.xls";
		PrintWriter out = new PrintWriter(new FileWriter(fileName));
		for(int i = 0; i < args.length; i++){
			out.println(args[i]);
		}
		out.close();
	}
}