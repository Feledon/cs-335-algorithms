package edu.uwec.cs.gehrkeb.assignment2;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class QuickHull implements ConvexHullFinder {

	@Override
	public List<Point2D> computeHull(List<Point2D> points) {
		// sort the points left to right
		Collections.sort(points, new Comparator<Point2D>() {
			@Override
			public int compare(Point2D p1, Point2D p2) {
				if(p1.getX() < p2.getX()) {
					return -1;
				} else if(p1.getX() > p2.getX()) {
					return 1;
				} 
				return 0;
			}
		});

		// find the leftmost and rightmost points
		Point2D leftMost = points.get(0);
		Point2D rightMost = points.get(points.size() - 1);

		// Construct a line connecting the leftmost to rightmost points
		Line2D.Double centerLine = new Line2D.Double(leftMost, rightMost);

		// Divide the points into 2 sets, those above the line and those below the line
		ArrayList<Point2D> topHalf = new ArrayList<Point2D>();
		ArrayList<Point2D> bottomHalf = new ArrayList<Point2D>();
		for(Point2D point : points) {
			if(centerLine.relativeCCW(point) > 0) {
				topHalf.add(point);
			} else if(centerLine.relativeCCW(point) < 0) {
				bottomHalf.add(point);
			}
		}

		//Call the recursive method, giving the line and the top set of points to produce the top half of the hull
		List<Point2D> topHull = recursiveQuickHull(centerLine, topHalf);
		centerLine = new Line2D.Double(centerLine.getP2(), centerLine.getP1());

		// Call the recursive method, giving the reversed line and the bottom set of points to produce the bottom half of the hull
		List<Point2D> bottomHull = recursiveQuickHull(centerLine, bottomHalf);

		// Glue the halves together
		if(!topHull.equals(bottomHull)) {
			topHull.addAll(bottomHull);
		}

		return topHull;
	}

	private List<Point2D> recursiveQuickHull(Line2D lineAB, List<Point2D> pointsAB) {
		List<Point2D> hull = new ArrayList<Point2D>();		
		if(pointsAB.isEmpty()) {// Base: no further points, return what was C
			if(lineAB.getP1().equals(lineAB.getP2())) {
				hull.add(lineAB.getP1());
				return hull;
			}
			hull.add(lineAB.getP2());
			return hull;	
		} else {
			// Get the farthest point from the line
			Point2D pointC = farthestPoint(lineAB, pointsAB);

			// Form line form B to C, Select point right of BC
			Line2D lineCB = new Line2D.Double(pointC, lineAB.getP2());
			List<Point2D> pointsCB = pointsLeftOf(lineCB, pointsAB);

			// Form line from A to C, Select points left of AC
			Line2D lineAC = new Line2D.Double(lineAB.getP1(), pointC);
			List<Point2D> pointsAC = pointsLeftOf(lineAC, pointsAB);

			// Combine the results of the subproblems into a solution for the large problem
			hull.addAll(recursiveQuickHull(lineCB, pointsCB));
			hull.addAll(recursiveQuickHull(lineAC, pointsAC));
		}

		return hull;
	}

	private Point2D farthestPoint(Line2D lineAB, List<Point2D> pointsAB) {
		Point2D farthestPoint = pointsAB.get(0);
		double farthestDistance = lineAB.ptLineDist(farthestPoint);
		for (Point2D point2D : pointsAB) {
			double distance = lineAB.ptLineDist(point2D);
			if (distance > farthestDistance) {
				farthestPoint = point2D;
				farthestDistance = distance;
			}
		}
		return farthestPoint;
	}

	private List<Point2D> pointsLeftOf(Line2D lineAB, List<Point2D> pointsAB) {
		ArrayList<Point2D> points = new ArrayList<Point2D>();
		for (Point2D point : pointsAB) {
			if (lineAB.relativeCCW(point) > 0) {
				points.add(point);
			}
		}
		return points;
	}

}
