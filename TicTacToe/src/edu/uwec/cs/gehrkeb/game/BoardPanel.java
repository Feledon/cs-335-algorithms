package edu.uwec.cs.gehrkeb.game;

import java.awt.Graphics;

import javax.swing.JPanel;

public class BoardPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * public void setBoard(TwoPlayerGameBoard newBoard) that tells it to display a new gameboard. 
	 * The GamePlayer is in charge of telling the BoardPanel when to display a new board. 
	 * The BoardPanel is just in charge of redrawing the current board on the screen.
	 * @param newBoard
	 */
	public void setBoard(TwoPlayerGameBoard newBoard){
		
	}
	
	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
	}

}
