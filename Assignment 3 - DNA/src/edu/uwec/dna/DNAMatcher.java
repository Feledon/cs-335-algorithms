package edu.uwec.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class DNAMatcher {
	Map<String, Integer> matchCost;
	private  String[] callBack;
	String text;
	String text2;

	String string1;
	String string2;
	int string1MatchStart;
	int string2MatchStart;
	int[][] optimal;

	/**
	 * Constructor
	 * @param matchCost
	 */
	public DNAMatcher(Map<String, Integer> matchCost) {
		this.matchCost = matchCost;
		this.string1MatchStart = 0;
		this.string2MatchStart = 0;
	}

	/**
	 * Main entry method that calls all the others.
	 * @param text
	 * @param text2
	 * @return
	 */
	public LocalAlignment findLocalAlignment(String text, String text2) {
		this.text = text2;
		this.text2 = text;
		this.string1 = text;
		this.string2 = text2;

		computeOptimalAlignmentTable();
		generateAlignmentFromTable();

		printOut(optimal, text, text2, callBack);
		LocalAlignment la = new LocalAlignment(string1, string2, string1MatchStart, string2MatchStart, this.string2.length());
		return la;
	}

	/**
	 * Create the table to find the values using threads and semaphores.
	 */
	private void computeOptimalAlignmentTable(){
		// Const 2d table
		optimal = new int[text.length()+1][text2.length()+1];
		final Semaphore[][] listOfSemaphore = new Semaphore[text.length() + 1][text2.length() + 1];

		for (int i = 0; i < text.length() + 1; i++) {
			optimal[i][0] = 0;
			listOfSemaphore[i][0] = new Semaphore(3);
		}

		for (int i = 1; i < text2.length() + 1; i++) { // already set 0,0
			optimal[0][i] = 0;
			listOfSemaphore[0][i] = new Semaphore(3);
		}

		List<Thread> threads = new ArrayList<Thread>();

		// Fill in the rest to the goal state
		for (int i=1; i<text.length()+1; i++) {
			for (int j=1; j<text2.length()+1; j++) {
				listOfSemaphore[i][j] = new Semaphore(0);
				final int x = i;
				final int y = j;

				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							listOfSemaphore[x - 1][y - 1].acquire(1);			
							listOfSemaphore[x - 1][y].acquire(1);
							listOfSemaphore[x][y - 1].acquire(1);

							int add = 0;
							//Add the amount for the current combination					
							add = matchCost.get(text.charAt(x - 1) + "" + text2.charAt(y - 1));
							int diag = optimal[x-1][y-1] + add + 1;

							add = matchCost.get(text.charAt(x - 1) + "-");
							int up = optimal[x-1][y] + add + 1;

							add = matchCost.get("-" + text2.charAt(y - 1));
							int left = optimal[x][y-1] + add + 1;

							int max = Math.max(up, Math.max(diag, left));

							if ((max + add) < 1) {
								optimal[x][y] = 0;
							}else{
								optimal[x][y] = max + add;
							}

							listOfSemaphore[x][y].release(3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				});
				threads.add(t);
			}
		}

		for (int i = 0; i < threads.size(); i++) {
			threads.get(i).start();
		}
		for (int i = 0; i < threads.size(); i++) {
			try {
				threads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Generate our output and lookback table
	 */
	private void generateAlignmentFromTable(){		
		int startIndex = setUpCallBack();//Get the index of our starting point in the last row, and initialize the table to all "L"
		int index = startIndex;

		for (int i = text.length(); i > 0; i--) {//Loop through the table, looking for the optimal path L, D, or U	
			String lookBack = "D"; //Default to the blank string?
			int diag = 0;
			int up = 0;	
			int left = 0;	
			
			if(startIndex != 0){
				diag = optimal[i-1][startIndex-1];
				up = optimal[i-1][startIndex];	
				left = optimal[i][startIndex-1];	
			}	

			if(diag == 0 && string1MatchStart == 0){
				string1MatchStart = index - 1;
				string2MatchStart = index - 1;
				for(int j = (callBack.length - (this.string2.length() + i + 1)); j > 0; j--){
					this.string2 = " " + this.string2;
				}
			}

			if(diag <= left && left >= up){
				lookBack = "L";
				startIndex = startIndex - 1;

				if(string1MatchStart != 0){
					i = 0;
				}else{
					this.string2 = string2.substring(0, i) + "-" + string2.substring(i);
					i++;
				}

			}else if(diag < up){
				lookBack = "U";
			}else{
				startIndex = startIndex - 1;
			}

			this.callBack[index] = lookBack;
			index--;
		}
	}

	/**
	 * Private helper to setup the defaults in callBack with all = to "L"
	 * and return the index of the largest value in the bottom row
	 * @return
	 */
	private int setUpCallBack(){
		callBack = new String[text2.length() + 1];
		//Load the table
		for(int i = 0; i < callBack.length; i++){
			callBack[i] = "L";
		}	

		//Find the starting point index
		int rowStart = optimal[text.length()][text2.length()];	
		int startIndex = text2.length();
		for (int j = text2.length(); j > 0; j--) {	
			int left = optimal[text.length()][j];
			if(left > rowStart){
				rowStart = left;
				startIndex = j;
			}
		}
		return startIndex;
	}

	/**
	 * Helper function to display out generated table
	 * @param optimalEDist
	 * @param text
	 * @param text2
	 */
	public static void printOut(int[][] optimalEDist,String text, String text2, String[] callBack) {
		int j = 0;
		System.out.print("-");
		System.out.print("\t");
		System.out.print("-");
		System.out.print("\t");
		for(int i = 0; i < text.length(); i++){
			System.out.print(text.charAt(i) +"");
			System.out.print("\t");
		}
		System.out.println();

		for(int[] row : optimalEDist) {  
			if(j == 0){
				System.out.print("-");
				System.out.print("\t");
			}else{
				System.out.print(text2.charAt(j - 1) +"");
				System.out.print("\t");	
			}
			j++;
			for (int i : row) {
				System.out.print(i);
				System.out.print("\t");
			}
			System.out.println();
		}  

		System.out.print(" \t");
		for(int i = 0; i < callBack.length; i++){
			System.out.print(callBack[i]);
			System.out.print("\t");
		}
		System.out.println();
	}
}

