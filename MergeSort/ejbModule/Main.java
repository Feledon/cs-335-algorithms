import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		int listSize = 88;

		for(int i = 1; i <= listSize; i++){
			list.add((int) (Math.random() * 100));
		}

		ArrayList<Integer> newList = mergeSort(list);
		for(int i = 0; i < newList.size(); i++){
			System.out.format("%d: %d -> %d \n",i, list.get(i),newList.get(i));
		}		
	}//END main

	/**
	 * @param inList
	 * @return Sorted list of items
	 */
	private static ArrayList<Integer> mergeSort(ArrayList<Integer> inList){
		ArrayList<Integer> outList = new ArrayList<Integer>();

		if(inList.size() <= 1){
			outList.add(inList.get(0));
		}else{			
			//Split in to two			
			ArrayList<Integer> left = mergeSort(new ArrayList<Integer>(inList.subList(0, (inList.size()/2))));			
			ArrayList<Integer> right = mergeSort(new ArrayList<Integer>(inList.subList((inList.size()/2),inList.size())));

			//Merge the two back together
			int l = 0;
			int r = 0;
			
			while(l < left.size() && r < right.size()){
				if(left.get(l).compareTo(right.get(r)) < 0 ){
					//Left is bigger
					outList.add(left.get(l));					
					l++;					
				}else{
					//Right is bigger
					outList.add(right.get(r));
					r++;
				}
			}

			if(left.size() > l){
				while(left.size() > l){
					outList.add(left.get(l));
					l++;
				}
			}else if(right.size() > r){
				while(right.size() > r){
					outList.add(right.get(r));
					r++;
				}
			}			
		}
		return outList;
	}//END mergeSort

}//END CLASS
