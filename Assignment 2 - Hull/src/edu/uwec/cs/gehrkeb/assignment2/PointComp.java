package edu.uwec.cs.gehrkeb.assignment2;

import java.awt.geom.Point2D;
import java.util.Comparator;

public class PointComp implements Comparator<Point2D>{

	public int compare(Point2D p1, Point2D p2) { // Ignoring duplicate X values...
		int result;
		if (p1.getX() < p2.getX())
			result = -1;
		else if (p1.getX() > p2.getX())
			result = 1;
		else{
			result = 0;
		}
		return result;
	}
}