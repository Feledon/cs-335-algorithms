package edu.uwec.dna;

import javax.swing.JTextField;
import javax.swing.text.Caret;

public class LocalAlignment {

	String string1; 
	String string2; 
	int string1MatchStart; 
	int string2MatchStart; 
	int matchLength;
	
	public void showAlignment(JTextField alignedString1Field, JTextField alignedString2Field) {
		alignedString1Field.setText(string1);
		Caret caret = alignedString1Field.getCaret();
		caret.setDot(string1MatchStart);
		caret.moveDot(string2.length());
		caret.setSelectionVisible(true);
		
		
		alignedString2Field.setText(string2);
		Caret caret2 = alignedString2Field.getCaret();
		caret2.setDot(string2MatchStart);
		caret2.moveDot(string2.length());
		caret2.setSelectionVisible(true);
	}
	
	public LocalAlignment(String string1, String string2, int string1MatchStart, int string2MatchStart, int matchLength){
		this.string1 = string1;
		this.string2 = string2;
		this.string1MatchStart = string1MatchStart;
		this.string2MatchStart = string2MatchStart;
		this.matchLength = matchLength;
	}

}
