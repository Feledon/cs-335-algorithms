package edu.uwec.cs.cs335.btframework;

public interface State {
	public boolean hasMoreChildren();
	public State nextChild();
	public boolean isFeasible();
	public boolean isSolved();
	public int getBound();
}
