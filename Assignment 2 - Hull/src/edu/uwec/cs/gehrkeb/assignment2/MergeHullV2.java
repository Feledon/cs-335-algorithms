package edu.uwec.cs.gehrkeb.assignment2;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MergeHullV2 implements ConvexHullFinder {
	@Override
	public List<Point2D> computeHull(List<Point2D> points) {
		// sort the points left to right
		Collections.sort(points, new Comparator<Point2D>() {
			@Override
			public int compare(Point2D p1, Point2D p2) {
				if(p1.getX() < p2.getX()) {
					return -1;
				} else if(p1.getX() > p2.getX()) {
					return 1;
				} 
				return 0;
			}
		});
		System.out.println("POINTS: " + points);
		List<Point2D> completeHull = recursiveMergeHull(points);
		System.out.println("COMPLETED HULL: " + completeHull);
		return completeHull;
	}

	/**
	 * Create our hull
	 * @param points
	 * @return
	 */
	private List<Point2D> recursiveMergeHull(List<Point2D> points) {
		List<Point2D> hull = new ArrayList<Point2D>();
		// Base: One or two points, must be a hull itself
		if(points.size() <= 3) {
			if(points.size() < 3) { // two points or one point
				hull.addAll(points);
			} else { // three points
				// create a tangent line and find the point .........
				Line2D tan = new Line2D.Double(points.get(0), points.get(points.size() - 1));

				if(tan.relativeCCW(points.get(1)) > 0) { // stores points as ccw
					Collections.reverse(points);			
				}
				return points;
			}

		} else {	
			List<Point2D> leftHull = recursiveMergeHull(points.subList(0, points.size() / 2));
			List<Point2D> rightHull = recursiveMergeHull(points.subList(points.size() / 2, points.size()));
			System.out.println("LEFT HULL: " + leftHull.toString());
			System.out.println("RIGHT HULL: " + rightHull.toString());

			// Find the rightmost point in the left hull
			Point2D rightMost = getRightMost(leftHull);
			System.out.println("RIGHT MOST: " + rightMost);

			// Find the leftmost point in the right hull
			Point2D leftMost = getLeftMost(rightHull);
			System.out.println("LEFT MOST: " + leftMost);

			Line2D startingTangent = new Line2D.Double(rightMost, leftMost);

			Line2D ending = new Line2D.Double();

			System.out.println("Starting Tangent: " + startingTangent.getP1() + " " + startingTangent.getP2());

			while(!(isLowerTangent(leftHull, startingTangent, rightMost)) && (!(isLowerTangent(rightHull, startingTangent, leftMost)))) {

				while(!(isLowerTangent(leftHull, startingTangent, rightMost))) {
					System.out.println("lower left");
					int indexA = leftHull.indexOf(rightMost);
					System.out.println("index A: " + indexA);
					if(indexA == 0) {
						rightMost = leftHull.get(leftHull.size() - 1);
					} else {
						rightMost = leftHull.get(indexA - 1);
					}

					ending.setLine(rightMost, leftMost);
					startingTangent.setLine(rightMost, leftMost);
				}
				
				System.out.println("A: " + rightMost);
				while(!(isLowerTangent(rightHull, startingTangent, leftMost))) {


					System.out.println("lower right");
					int indexB = rightHull.indexOf(leftMost);
					System.out.println("index B: " + indexB);
					if(indexB >= rightHull.size() - 1) {
						leftMost = rightHull.get(0);

					} else {
						leftMost = rightHull.get(indexB + 1);
					}

					ending.setLine(rightMost, leftMost);
					startingTangent.setLine(rightMost, leftMost);

					System.out.println("B: " + leftMost);

				}

				System.out.println("A: " + rightMost + "B: " + leftMost);

			}



			//	        System.out.println(bottomLine.getP1() + " " + bottomLine.getP2());
			System.out.println("Bottom Line: " + startingTangent.getP1() + " " + startingTangent.getP2());
			//	        Line2D end = new Line2D.Double(leftMost, rightMost);
			
			Collections.reverse(leftHull);
			Collections.reverse(rightHull);
			Line2D end = new Line2D.Double(leftMost, rightMost);
			Line2D topLine = walkTheLine(leftHull, rightHull, rightMost, leftMost, end);
			System.out.println("TOP LINE: " + topLine.getP1() + " " + topLine.getP2());
			
			hull.add(topLine.getP1());
			hull.add(topLine.getP2());
			//	        Line2D topLine = walkTheLine(rightHull, leftHull, a, b, end);
			//	        System.out.println(topLine.getP1() + " " + topLine.getP2());

			//	  
			//	        System.out.println(end.getP1() + " " + end.getP2());
			//	        hull.add(startingTangent.getP1());
			//	        hull.add(startingTangent.getP2());
			//	        hull.add(topLine.getP1());
			//	        hull.add(topLine.getP2());
			////	        
			//	       int topLeftIndex = leftHull.indexOf(startingTangent.getP1());
			//	       int bottomLeftIndex = leftHull.indexOf(startingTangent.getP2());
			//	       for(int i = topLeftIndex + 1; i < bottomLeftIndex; i++) {
			//	    	   hull.add(leftHull.get(i));
			//	       }
			//	       
			//	       int topRightIndex = rightHull.indexOf(topLine.getP1());
			//	       int bottomRightIndex = rightHull.indexOf(topLine.getP2());
			//	       for(int i = topRightIndex + 1; i < bottomRightIndex; i++) {
			//	    	   hull.add(rightHull.get(i));
			//	       }


		}
		return hull;

	}

	private Line2D walkTheLine(List<Point2D> leftHull, List<Point2D> rightHull, Point2D rightMost, Point2D leftMost, Line2D startingTangent) {
		// Connect right and left most to form starting tangent
		Line2D ending = new Line2D.Double();

		System.out.println("Starting Tangent: " + startingTangent.getP1() + " " + startingTangent.getP2());

		while(!(isLowerTangent(leftHull, startingTangent, rightMost)) && (!(isLowerTangent(rightHull, startingTangent, leftMost)))) {

			while(!(isLowerTangent(leftHull, startingTangent, rightMost))) {

				System.out.println("lower left");
				int indexA = leftHull.indexOf(rightMost);
				System.out.println("index A: " + indexA);
				if(indexA == 0) {
					rightMost = leftHull.get(leftHull.size() - 1);
				} else {
					rightMost = leftHull.get(indexA - 1);
				}

				ending.setLine(rightMost, leftMost);
				startingTangent.setLine(rightMost, leftMost);



			}
			System.out.println("A: " + rightMost);
			while(!(isLowerTangent(rightHull, startingTangent, leftMost))) {


				System.out.println("lower right");
				int indexB = rightHull.indexOf(leftMost);
				System.out.println("index B: " + indexB);
				if(indexB >= rightHull.size() - 1) {
					leftMost = rightHull.get(0);

				} else {
					leftMost = rightHull.get(indexB + 1);
				}

				ending.setLine(rightMost, leftMost);
				startingTangent.setLine(rightMost, leftMost);

				System.out.println("B: " + leftMost);

			}

			System.out.println("A: " + rightMost + "B: " + leftMost);

		}
		return startingTangent;

	}

	private boolean isLowerTangent(List<Point2D> hull, Line2D AB, Point2D p) {
		boolean isLowerTangent = false;
		int index = hull.indexOf(p);

		int prev = 0;
		if((index - 1) < 0) {
			prev = hull.size() - 1;
		} else {
			prev = index - 1;
		}

		int next = 0;
		if((index + 1) >= hull.size()) {
			next = 0;
		} else {
			next = index + 1;
		}	

		System.out.println("PREV: " + prev + " NEXT: " + next);

		if((AB.relativeCCW(hull.get(prev)) == 1) && (AB.relativeCCW(hull.get(next)) == 1)) {
			isLowerTangent = true;
		}

		return isLowerTangent;
	}

	private Point2D getRightMost(List<Point2D> leftHull) {
		if(!leftHull.isEmpty()) {
			Point2D rightMost = Collections.max(leftHull, new Comparator<Point2D>() {
				@Override
				public int compare(Point2D p1, Point2D p2) {
					if(p1.getX() < p2.getX()) {
						return -1;
					} else if(p1.getX() > p2.getX()) {
						return 1;
					} 
					return 0;
				}
			});
			return rightMost;
		} else {
			return null;
		}	

	}

	private Point2D getLeftMost(List<Point2D> rightHull) {
		if(!rightHull.isEmpty()) {
			Point2D leftMost = Collections.min(rightHull, new Comparator<Point2D>() {
				@Override
				public int compare(Point2D p1, Point2D p2) {
					if(p1.getX() < p2.getX()) {
						return -1;
					} else if(p1.getX() > p2.getX()) {
						return 1;
					} 
					return 0;
				}
			});
			return leftMost;
		} else {
			return null;
		}
	}

}