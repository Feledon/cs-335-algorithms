package edu.uwec.cs.cs335.btframework;

import java.util.ArrayList;
import java.util.List;


public class GraphColoringMain {

	public static void main(String[] args) { 
		Backtracker bt = new Backtracker(); 
		// Define the graph from the ppt 
		//		boolean[][] graph = 
		//			{{false, true, false, false, false, true}, 
		//				{ true, false, true, false, false, true}, 
		//				{false, true, false, true, true, false}, 
		//				{false, false, true, false, true, false}, 
		//				{false, false, true, true, false, true}, 
		//				{ true, true, false, false, true, false}}; 

		boolean[][] graph = 
			{{false, true, true},
			 {true, false, true},
			 {true, true, false}};

		// Define the colors used in the ppt
		List<ColorNode> colors = new ArrayList<ColorNode>(); 
		colors.add(new ColorNode(2, "Red"));
		colors.add(new ColorNode(3, "Green"));
		colors.add(new ColorNode(5, "Blue"));
		colors.add(new ColorNode(2, "Yellow"));

		GraphColoringState s = new GraphColoringState(graph, colors); 
		//State result = bt.backtrack(s); 
		State result = bt.backtrackBest(s); 
		System.out.println(result);
		s = new GraphColoringState(graph, colors);
		result = bt.backtrackBreadth(s); 
		System.out.println(result);
		s = new GraphColoringState(graph, colors);
		result = bt.backtrackDepth(s); 
		System.out.println(result);
	}
}
