package edu.uwec.cs.gehrkeb.path;

import java.awt.Point;
import java.io.File;

import javax.swing.JFileChooser;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        String path = "A Star test.txt";

		TerrainMap tm = new TerrainMap(path);
		TerrainPanel tp = new TerrainPanel(tm);
		Avatar ava = new Avatar(new Point(0, 0), tp);
		tp.addAvatar(ava);
	}

}
