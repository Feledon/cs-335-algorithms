package edu.uwec.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class TestOne {
	private Map<String, Integer> matchCost;
	private int largest = 0;
	private int largestX = 0;
	private int largesty = 0;
	private int[][] paths;

	public TestOne(Map<String, Integer> matchCost) {
		this.matchCost = matchCost;
	}

	public LocalAlignment findLocalAlignment(String text, String text2) {
		int[][] results = generateTablesThreaded(text, text2);
		String finalTop = "";
		String finalBot = "";
		// 1 2
		// 0 3
		boolean loop = true;
		int x = largestX;
		int y = largesty;
		int length = 0;
		while (loop) {
			if (paths[x][y] != 3) {
				if (paths[x][y] == 0) {
					finalTop = text.substring(x - 1, x) + finalTop;
					finalBot = "-" + finalBot;

					x--;
				} else if (paths[x][y] == 1) {
					finalTop = text.substring(x - 1, x) + finalTop;
					finalBot = text2.substring(y - 1, y) + finalBot;
					x--;
					y--;
				} else if (paths[x][y] == 2) {
					finalTop = "-" + finalTop;
					finalBot = text2.substring(y - 1, y) + finalBot;
					y--;
					length++;
				}

			} else {
				loop = false;
			}

		}

		int difference = Math.abs(text.substring(0, x).length() - text2.substring(0, y).length());
		finalTop = text.substring(0, x) + finalTop + text.substring(largestX);
		finalBot = text2.substring(0, y) + finalBot + text2.substring(largesty);
		System.out.println(largestX - x);

		// where to start highlighting on top string
		int topHighlight = 0;
		if (finalTop.length() > finalBot.length()) {
			topHighlight = x;
		} else {
			topHighlight = x + difference;
		}

		// where to start highlighting on bottom string
		int botHighlight = 0;
		if (finalTop.length() < finalBot.length()) {
			botHighlight = y;
		} else {
			botHighlight = y + difference;
		}
		if (finalTop.length() > finalBot.length()) {
			for (int i = 0; i < difference; i++) {
				finalBot = " " + finalBot;
			}
		} else {
			for (int i = 0; i < difference; i++) {
				finalTop = " " + finalTop;
			}
		}

		// how far to highlight
		int highlightDistance = largestX - x + length;
		LocalAlignment align = new LocalAlignment(finalTop, finalBot, topHighlight, botHighlight, highlightDistance);

		System.out.println(printTables(text, text2, results));
		return align;
	}

	private String printTables(String text, String text2, int[][] table) {
		String out = "";
		for(int i=0; i< text2.length()+1; i++){
			for(int j=0; j<text.length()+1; j++){
				out += table[j][i] + "\t";
			}
			out+="\n";
		}
		return out;
	}

	private int[][] generateTablesThreaded(final String word1, final String word2) {
		final int[][] optimal = new int[word1.length() + 1][word2.length() + 1];
		final int[][] path = new int[word1.length() + 1][word2.length() + 1];
		paths = path;

		// Fill in the base case values
		// x row and x col
		final Semaphore[][] listOfSemaphore = new Semaphore[word1.length() + 1][word2.length() + 1];
		for (int i = 0; i < word1.length() + 1; i++) {
			path[i][0] = 3;
			optimal[i][0] = 0;
			listOfSemaphore[i][0] = new Semaphore(3);
		}

		for (int i = 1; i < word2.length() + 1; i++) { // already set 0,0
			path[0][i] = 3;
			optimal[0][i] = 0;
			listOfSemaphore[0][i] = new Semaphore(3);
		}

		List<Thread> threads = new ArrayList<Thread>();

		for (int i = 1; i < word1.length() + 1; i++) {
			for (int j = 1; j < word2.length() + 1; j++) {
				final int x = i;
				final int y = j;
				listOfSemaphore[x][y] = new Semaphore(0);
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						try {

							listOfSemaphore[x - 1][y - 1].acquire(1);
							listOfSemaphore[x - 1][y].acquire(1);
							listOfSemaphore[x][y - 1].acquire(1);

							// i=x=word1
							// j=y=word2

							String left = word1.substring(x - 1, x) + "-";
							int value1 = optimal[x - 1][y] + matchCost.get(left);

							String topleft = word1.substring(x - 1, x) + word2.substring(y - 1, y);
							int value2 = optimal[x - 1][y - 1] + matchCost.get(topleft);

							String top = "-" + word2.substring(y - 1, y);
							int value3 = optimal[x][y - 1] + matchCost.get(top);

							// Select the max of these values
							int max = Math.max(value1, Math.max(value2, Math.max(0, value3)));
							optimal[x][y] = max;
							if (max > largest) {
								largest = max;
								largestX = x;
								largesty = y;
							}

							// 1 2
							// 0 3
							// for directions
							// 3 means the end

							if (value2 == max) {
								path[x][y] = 1;
							} else if (value3 == max) {
								path[x][y] = 2;
							} else if (value1 == max) {
								path[x][y] = 0;
							} else {
								path[x][y] = 3;
							}

							listOfSemaphore[x][y].release(3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				});
				threads.add(t);

			}
		}
		for (int i = 0; i < threads.size(); i++) {
			threads.get(i).start();
		}
		for (int i = 0; i < threads.size(); i++) {
			try {
				threads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return optimal;
	}

}