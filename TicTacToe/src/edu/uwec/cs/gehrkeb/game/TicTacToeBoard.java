package edu.uwec.cs.gehrkeb.game;

import java.awt.Graphics;
import java.awt.geom.Point2D;

/**
 *code just the constructors, hasMoreChildren, nextChild, staticEvaluation, and toString methods. 
 *Now, before you continue onward, test these methods to make sure they work. 
 *That is, create a TestCase of some sort that creates a new TicTacToe board and then ask it for children or evaluations, etc. 
 *Make sure it works before you continue onto to the next part. 
 *You will probably have a hard time using asserts, but at a minimum you can print out the resulting children to make sure they are correct. 
 *The advantage of a TestCase is that you can save the tests to be able to rerun them later. 
 *However, the TestCase is not required for this assignment � just a suggestion
 * @author gehrkeb
 *
 */
public class TicTacToeBoard implements TwoPlayerGameBoard {
	private int[] board;
	private boolean isPlayerMove;
	int nextOpenPosition;	

	public TicTacToeBoard(int boardSize){
		this.board = new int[boardSize];
		this.isPlayerMove = true;  //Default to player first move
		nextOpenPosition = 0;
		for (int i=0; i<board.length; i++) {
			this.board[i] = 3;
		}
	}

	public TicTacToeBoard(TicTacToeBoard game){
		this.board = game.board;
		this.isPlayerMove = game.isPlayerMove;		
		this.nextOpenPosition = game.nextOpenPosition;
		for (int i=0; i<board.length; i++) {
			this.board[i] = game.board[i];
		}				
	}

	/**
	 * Test constructor taking in a partially completed board
	 * @param board
	 */
	public TicTacToeBoard(int[] board){
		this.board = board;
		this.isPlayerMove = true;
		nextOpenPosition = getNextOpenPosition(0);
	}

	private int getNextOpenPosition(int start){
		int openSpot = -1;
		for(int i = start; (i < board.length) && (openSpot == -1); i++){
			if(board[i] == 3){
				openSpot = i;
			}
		}
		return openSpot;
	}

	@Override
	public boolean hasMoreChildren() {
		return (nextOpenPosition > -1);
	}

	@Override
	public TwoPlayerGameBoard nextChild() {
		TicTacToeBoard child = new TicTacToeBoard(this);		
		child.board[this.nextOpenPosition] = this.isPlayerMove?1:0;
		child.nextOpenPosition = child.getNextOpenPosition(nextOpenPosition + 1);
		
		return child;
	}

	@Override
	public double staticEvaluation() {
		double score = colWin(this.board, isPlayerMove) + rowWin(this.board, isPlayerMove) + diagWin(this.board, isPlayerMove);

		return score;
	}

	public double diagWin(int[] board, boolean isPlayerMove){
		double isWin = 0;		
		int playerSymbol = isPlayerMove?1:0;
		
		if(board[0] == playerSymbol && board[4] == playerSymbol && board[8] == playerSymbol ||
		   board[2] == playerSymbol && board[4] == playerSymbol && board[6] == playerSymbol){
			isWin = 1;
		}
		return isWin;
	}

	public double colWin(int[] board, boolean isPlayerMove){
		double isWin = 0;
		int mod = (int) Math.sqrt(board.length);
		int playerSymbol = isPlayerMove?1:0;
		for(int i = 0; i < mod; i++){
			if(board[i] == playerSymbol && board[i + mod] == playerSymbol && board[i + 2*mod] == playerSymbol){
				isWin = 1;
			}
		}
		return isWin;
	}

	public double rowWin(int[] board, boolean isPlayerMove){
		double isWin = 0;
		int mod = (int) Math.sqrt(board.length);
		int playerSymbol = isPlayerMove?1:0;

		for(int i = 0; i < board.length; i+=mod){
			if(board[i] == playerSymbol && board[i + 1] == playerSymbol && board[i + 2] == playerSymbol){
				isWin = 1;
			}
		}
		return isWin;
	}

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isComputerWinner() {
		boolean isWin = false;
		if(rowWin(board, false) > 0 || colWin(board, false) > 0 || diagWin(board, false) > 0){
			isWin = true;
		}
		return isWin;
	}

	@Override
	public boolean isDraw() {
		boolean isDraw = false;
		if(!isComputerWinner() && !isUserWinner()){
			isDraw = true;
		}
		return isDraw;
	}

	@Override
	public boolean isUserWinner() {
		boolean isWin = false;
		if(rowWin(board, true) > 0 || colWin(board, true) > 0 || diagWin(board, true) > 0){
			isWin = true;
		}
		return isWin;
	}

	@Override
	public void placeUserMove(Point2D mouseLocation) throws Exception {
		// TODO Auto-generated method stub

	}

	public String toString() {
		String result = "";
		for (int i=0; i<board.length; i++) {
			if(i % Math.sqrt(board.length) == 0){
				result += "\n";
			}
			if(board[i] == 0){
				result += "O ";
			}else if(board[i] == 1){
				result += "X ";
			}else{
				result += "- ";
			}			
		}		
		result += "\nNext Open: " + nextOpenPosition;
		result += "\nStatic Eval: " + staticEvaluation();
		result +=  "\nComputer Wins: " + isComputerWinner() + "\nPlayer Wins: " +  isUserWinner() + "\nNobody Wins: " + isDraw();
		return result;
	}


}
