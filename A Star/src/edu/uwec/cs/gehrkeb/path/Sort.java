package edu.uwec.cs.gehrkeb.path;

public class Sort {

	public static void main(String[] args) {
		int[] arr = {12,11,55,-1};
		int x = 1;
		int y = 1;

		while(arr[x] != -1){
			while(arr[y] != -1){				
				if(arr[y] != -1 && arr[y - 1] > arr[y]){
					int temp = arr[y - 1];
					arr[y - 1] = arr[y];
					arr[y] = temp;
				}
				
				y++;
			}
			x++;
		}

		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + ":");
		}
	}

}
