package dynprog;

public class MMult {

	public MMult() {
	}
	
	public int findOptimal(int[] dims) {
		
		// Build my empty 2d table
		int n = dims.length - 1;
		int[][] optimalMult = new int[n][n];
		
		// Fill in base cases
		for (int i=0; i<n; i++) {
			optimalMult[i][i] = 0;
		}
		
		// Fill in the rest of the table to the goal
		// Diag by diag
		for (int diagCol=1; diagCol < n; diagCol++) {
			
			for (int diagNum=0; diagNum < (n-diagCol); diagNum++) {
				
				int i = diagNum;
				int j = diagCol + diagNum;
				
				// Find min of the options
				
				
			}
			
		}
		
		return -1;
	}
	
	public static void main(String[] args) {
		MMult mm = new MMult();
		
		int[] dims = new int[7];
		dims[0] = 5;
		dims[1] = 2;
		dims[2] = 3;
		dims[3] = 4;
		dims[4] = 6;
		dims[5] = 7;
		dims[6] = 8;
		
		int result = mm.findOptimal(dims);
		System.out.println(result);

	}

}
