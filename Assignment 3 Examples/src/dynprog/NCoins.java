package dynprog;

import java.util.*;

public class NCoins {
	
	private List<Integer> minCoins;
	private List<Integer> coinUsed;

	public NCoins(List<Integer> coins) {
	
		minCoins = new ArrayList<Integer>();
		coinUsed = new ArrayList<Integer>();
		
		// Fill in the base cases
		minCoins.add(0);  // 0 coins in the 0th slot
		coinUsed.add(0);
		
		for (int i=1; i<=100; i++) {
			
			int minSoFar = Integer.MAX_VALUE;
			int minCoinSoFar = -1;
			
			for (int j=0; j<coins.size(); j++) {
				int currentCoin = coins.get(j);
				
				if ((i - currentCoin) >= 0) {
					int tableValue = minCoins.get(i-currentCoin);
					
					if ((tableValue + 1) < minSoFar) {
						minSoFar = (tableValue + 1);
						minCoinSoFar = coins.get(j);
					}
				}
			}
			
			minCoins.add(minSoFar);
			coinUsed.add(minCoinSoFar);
		}
		
	}
	
	int findMin(int n) {
		
		return minCoins.get(n);
	}
	
	List<Integer> findChange(int n) {
	
		List<Integer> coins;
		if (n == 0) {
			coins = new ArrayList<Integer>();
			
		} else {
			coins = findChange(n - coinUsed.get(n));
		
			coins.add(coinUsed.get(n));
		}		
		
		return coins;
	}
	
	public static void main(String[] args) {
		
		List<Integer> coins = new ArrayList<Integer>();
		coins.add(25);
		coins.add(12);
		coins.add(10);
		coins.add(5);
		coins.add(1);
		
		NCoins nc = new NCoins(coins);
		
		System.out.println(nc.findMin(29));
		System.out.println(nc.findChange(29));

	}

}
