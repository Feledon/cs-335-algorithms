package edu.uwec.cs.muotkaht.game;

public class MiniMax {
	
	private int maxLevel;
	
	public MiniMax(int maxLevel) {
		this.maxLevel = maxLevel;
	}
	
	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}
	
	public TwoPlayerGameBoard generateNextMove(TwoPlayerGameBoard currentProblem) {
		TwoPlayerGameBoard nextMove = null;
		TwoPlayerGameBoard child;
		double bestChildEval = -Double.MAX_VALUE;
		double childEval;
		
		// Child with highest minimax
		while (currentProblem.hasMoreChildren()) {
			child = currentProblem.nextChild();
			childEval = recursiveMiniMaxAlphaBeta(child, 1, -Double.MAX_VALUE, Double.MAX_VALUE);
			
			if (childEval > bestChildEval) {
				nextMove = child;
				bestChildEval = childEval;
			}
		}
		return nextMove;
	}
	
	private double recursiveMiniMaxAlphaBeta(TwoPlayerGameBoard currentProblem, int currentLevel, double alpha, double beta) {
		// Search all levels possible
		if (currentLevel == maxLevel || currentProblem.isDraw() || currentProblem.isComputerWinner() || currentProblem.isUserWinner()) {
			return currentProblem.staticEvaluation();
		}
				
		TwoPlayerGameBoard child;
		child = currentProblem.nextChild();
		
		// Maximizing level
		if (currentLevel % 2 == 0) {
			//Get the first max child for alpha pruning.
			alpha = Math.max(alpha, recursiveMiniMaxAlphaBeta(child, currentLevel + 1, alpha, beta));
			
			while (currentProblem.hasMoreChildren() && alpha < beta) {
				child = currentProblem.nextChild();
				alpha = Math.max(alpha, recursiveMiniMaxAlphaBeta(child, currentLevel + 1, alpha, beta));
			}
			
			return alpha;
		// Minimizing level
		} else {			
			beta = Math.min(beta, recursiveMiniMaxAlphaBeta(child, currentLevel + 1, alpha, beta));
			
			while (currentProblem.hasMoreChildren() && alpha < beta) {
				child = currentProblem.nextChild();
				beta = Math.min(beta, recursiveMiniMaxAlphaBeta(child, currentLevel + 1, alpha, beta));
			}
			
			return beta;
		}
	}
}