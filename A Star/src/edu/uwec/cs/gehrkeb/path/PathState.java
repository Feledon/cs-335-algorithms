package edu.uwec.cs.gehrkeb.path;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * State controller for managing and producing children for the A star algorithm.
 * @author gehrkeb
 *
 */
public class PathState implements Comparable<PathState> {
	private List<Point> returnPoints;
	private Point start;
	private Point end;
	int childNum;
	int distance;
	TerrainMap tm;
	Point currentPoint;
	int costSoFar;
	int estCost;
	int LOWEST_LAND_COST = 2;
	
	public PathState(PathState current, Point currentP){
		this.start = current.start;
		this.end = current.end;
		this.childNum = 4;
		this.distance = current.distance - 1; //Manhattan distance to goal
		this.tm = current.tm;
		this.currentPoint = currentP;
		this.costSoFar = current.costSoFar;
		this.estCost = costSoFar + (distance * LOWEST_LAND_COST);
		
		this.returnPoints = new ArrayList<Point>(current.returnPoints);
	
		if(!this.returnPoints.contains(currentP)){
			this.returnPoints.add(currentP);	
		}	
	}
	
	// You need to decide what things to pass the constructor
	public PathState(Point start, Point end, TerrainMap tm) {
		this.returnPoints = new ArrayList<Point>();
		returnPoints.add(start);
		
		this.start = start;
		this.end = end;
		this.childNum = 4;
		distance = (Math.abs(end.x - start.x) + Math.abs(end.y - start.y)); //Manhattan distance to goal * lowest bounding cost
		this.tm = tm;
		this.currentPoint = start;
		this.costSoFar = 0;
		this.estCost = LOWEST_LAND_COST * distance;
	}

	public boolean hasMoreChildren() {
		boolean ret = true;
		if(this.currentPoint.x - 1 < 0 && childNum == 2) {
			ret = false;
		}else if(this.currentPoint.y - 1 < 0 && childNum == 1){
			ret = false;
		}else if(this.currentPoint.x + 1 >= 20 && childNum == 4){
			ret = false;
		}else if(this.currentPoint.y + 1 >= 20 && childNum == 3){
			ret = false;
		}else if(childNum <= 0){			
			ret = false;
		}
		return ret;
	}
	
	
	public PathState getNextChild() {
		Point childPoint = new Point(currentPoint.x + 1, currentPoint.y);
		switch(childNum){
		case 3://bottom child
			childPoint = new Point(currentPoint.x, currentPoint.y + 1);
			break;
		case 2://left child
			childPoint = new Point(currentPoint.x - 1, currentPoint.y);
			break;
		case 1://top child
			childPoint = new Point(currentPoint.x, currentPoint.y - 1);
			break;
		}			
		
		PathState child = new PathState(this, childPoint);
		this.childNum -= 1;	
		child.costSoFar += tm.getCost(child.currentPoint);	
		return child;
	}
	
	public boolean isGoal() {
		return (currentPoint.equals(end));		
	}
	
	public int getBound(){
		return costSoFar;
	}
	
	// This will give you a list of the path from the start to the current position
	public List<Point> getPath() {
		return returnPoints;	
	}
	
	
	// You may need equals/compareto/hashcode/tostring depend on the data structures you are using
	/**
	 * Creates a string of the current path
	 */
	public String toString(){
		String ret = "Est Cost=" + this.estCost + " X=" + currentPoint.getX() + " Y=" + currentPoint.getY();
		return ret;		
	}
	
	// This assumes that you have a Point called position as an instance variable, but feel free to change this method as well
	public void draw(Graphics g, int width, int height) {
		Point position = this.currentPoint;
		double estimate = this.estCost;
		
		// Shade based on the estimator
		int colorShade = (int)((estimate) * 1.5);
		if (colorShade > 255) {
			colorShade = 255;
		}
		
		g.setColor(new Color(colorShade, colorShade, colorShade));
		
		g.fillOval((position.x * width) + (width/4), (position.y * height) + (width/4), width/2, height/2);
		
		g.setColor(Color.black);
		g.drawOval((position.x * width) + (width/4), (position.y * height) + (width/4), width/2, height/2);
	}

	@Override
	public boolean equals(Object p){
		PathState ps = (PathState) p;
		return this.currentPoint.x == ps.currentPoint.x && this.currentPoint.y == ps.currentPoint.y ;		
	}
	
	@Override
	public int compareTo(PathState o) {
		int ret = 1;
		if(this.estCost == o.estCost ){
			ret = 0;
		}else if(this.estCost < o.estCost){
			ret = -1;
		}
		return ret;
	}
}

