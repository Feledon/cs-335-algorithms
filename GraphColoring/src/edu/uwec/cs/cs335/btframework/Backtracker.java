package edu.uwec.cs.cs335.btframework;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class Backtracker {

	public State backtrack(State s) {
		System.out.println(s);
		State result = null;
		
		// Base case
		if (s.isSolved()) {
			result = s;
		} else {    // Recursive case
			
			// Bail out when we find a single solution
			while (s.hasMoreChildren() && (result == null)) {
				State child = s.nextChild();
				//System.out.println("-->" + child);
				if (child.isFeasible()) {
					result = backtrack(child);
				}
			}
		}
		
		return result;
	}
	
	public State backtrackBest(State s) {
		System.out.print("Best-First: ");
		// This is used to keep track of the number of states for which we need to examine 
		int numberOfStatesExpanded = 0;

		State bestSolution = null;

		// Initialize the statesToProcess data structure
		PriorityQueue<State> statesToProcess = new PriorityQueue<State>();  // Best-first
		statesToProcess.add(s);

		// While there are still states to process
		while (!statesToProcess.isEmpty() ) {
			// Remove the next state from the data structure
			State currentState = statesToProcess.poll();  // Best-first
			numberOfStatesExpanded++;

//			System.out.println(currentState);

			// Check to see if the state is solved
			if (currentState.isSolved()) {
				// Replace the best solution if the current solution is better
				if (bestSolution == null || currentState.getBound() < bestSolution.getBound()) {
					bestSolution = currentState;
				}
			} else { // The state is not solved
				// Check to see if we need to continue expanding the state
				if ( bestSolution == null || (currentState.getBound() < bestSolution.getBound()) ) {
					// Expand the state by producing all its children
					while (currentState.hasMoreChildren()) {
						State childState = currentState.nextChild();
						// Add each child that is feasible to the data structure
						if (childState.isFeasible()) {
							//									System.out.println(childState);
							statesToProcess.add(childState);  // Breadth-first, Best-first
							//statesToProcess.add(0, childState);  // Depth-first
						}
					}
				} else {
					// For Best-first only
					statesToProcess.clear();            		
				}
			}
		}

		System.out.println(numberOfStatesExpanded);
		return bestSolution;
	}	


	// Iterative version that includes bound pruning
	public State backtrackBreadth(State s) {
		System.out.print("Breadth-First: ");
		// This is used to keep track of the number of states for which we need to examine 
		int numberOfStatesExpanded = 0;

		State bestSolution = null;

		// Initialize the statesToProcess data structure
		List<State> statesToProcess = new LinkedList<State>();  // Breadth-first, Depth-first
		statesToProcess.add(s);

		// While there are still states to process
		while (!statesToProcess.isEmpty() ) {
			// Remove the next state from the data structure
			 State currentState = statesToProcess.remove(0);  // Breadth-first, Depth-first
			numberOfStatesExpanded++;
//			System.out.println(currentState);

			// Check to see if the state is solved
			if (currentState.isSolved()) {
				// Replace the best solution if the current solution is better
				if (bestSolution == null || currentState.getBound() < bestSolution.getBound()) {
					bestSolution = currentState;
				}

			} else { // The state is not solved
				// Check to see if we need to continue expanding the state
				if ( bestSolution == null || (currentState.getBound() < bestSolution.getBound()) ) {
					// Expand the state by producing all its children
					while (currentState.hasMoreChildren()) {
						State childState = currentState.nextChild();

						// Add each child that is feasible to the data structure
						if (childState.isFeasible()) {
							//							System.out.println(childState);
							statesToProcess.add(childState);  // Breadth-first, Best-first
							//statesToProcess.add(0, childState);  // Depth-first
						}
					}
				} 
			}
		}
		System.out.println(numberOfStatesExpanded);
		return bestSolution;
	} 
	
	public State backtrackDepth(State s) {
		System.out.print("Breadth-First: ");
		// This is used to keep track of the number of states for which we need to examine 
		int numberOfStatesExpanded = 0;

		State bestSolution = null;

		// Initialize the statesToProcess data structure
		List<State> statesToProcess = new LinkedList<State>();  // Breadth-first, Depth-first
		statesToProcess.add(s);

		// While there are still states to process
		while (!statesToProcess.isEmpty() ) {
			// Remove the next state from the data structure
			 State currentState = statesToProcess.remove(statesToProcess.size() - 1);  // Breadth-first, Depth-first
			numberOfStatesExpanded++;
//			System.out.println(currentState);

			// Check to see if the state is solved
			if (currentState.isSolved()) {
				// Replace the best solution if the current solution is better
				if (bestSolution == null || currentState.getBound() < bestSolution.getBound()) {
					bestSolution = currentState;
				}

			} else { // The state is not solved
				// Check to see if we need to continue expanding the state
				if ( bestSolution == null || (currentState.getBound() < bestSolution.getBound()) ) {
					// Expand the state by producing all its children
					while (currentState.hasMoreChildren()) {
						State childState = currentState.nextChild();

						// Add each child that is feasible to the data structure
						if (childState.isFeasible()) {
							//							System.out.println(childState);
							statesToProcess.add(childState);  // Breadth-first, Best-first
							//statesToProcess.add(0, childState);  // Depth-first
						}
					}
				} 
			}
		}
		System.out.println(numberOfStatesExpanded);
		return bestSolution;
	}         
}
