package dynprog;

public class EdDist {

	public EdDist() {	
	}
	
	public int findDist(String word1, String word2) {
		
		// Const 2d table
		int[][] optimalEDist = new int[word1.length()+1][word2.length()+1];
		
		// Fill in base cases
		for (int i=0; i<word1.length()+1; i++) {
			optimalEDist[i][0] = i;
		}
		
		for (int i=1; i<word2.length()+1; i++) {
			optimalEDist[0][i] = i;
		}
		
		// Fill in the rest to the goal state
		for (int i=1; i<word1.length()+1; i++) {
			for (int j=1; j<word2.length()+1; j++) {
				
				int diag = optimalEDist[i-1][j-1];
				if (word1.charAt(i-1) != word2.charAt(j-1)) {
					diag++;
				}
				
				int up = optimalEDist[i-1][j] + 1;
				int left = optimalEDist[i][j-1] + 1;
				
				int min = Math.min(up, Math.min(diag, left));
				optimalEDist[i][j] = min;
			}
		}
		
		
		return optimalEDist[word1.length()][word2.length()];
	}
	
	public static void main(String[] args) {
		EdDist ed = new EdDist();
		int result = ed.findDist("cat", "cake");
		System.out.println(result);
	}

}
