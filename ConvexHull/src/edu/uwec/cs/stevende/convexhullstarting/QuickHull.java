package edu.uwec.cs.stevende.convexhullstarting;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Find left/right, do top half first
 * pivot = farthest from left-right line towards top
 * then bottom half
 * 
 * @author gehrkeb
 *
 */
public class QuickHull extends ConvexHullFinder {
	
	private List<Point2D> finalList = new ArrayList<Point2D>();

	public List<Point2D> computeHull(List<Point2D> points) {
		//		display(points);
		Collections.sort(points, new CoordinateComparator());
		
//		List<Point2D> left = points.subList(0, points.size()/2);
		//display(left);
		Line2D.Double line = new Line2D.Double(points.get(0), points.get(points.size() - 1));
		List<Point2D> left = getAboveLine(points, line);
		
		left = buildHull(left, line);

//		List<Point2D> right = buildHull(points.subList(points.size()/2, points.size() - 1), line);

		//		display(points);

		return finalList;
	}

	private List<Point2D> getAboveLine(List<Point2D> points, Line2D.Double line){
		List<Point2D> above = new ArrayList<Point2D>();
		
		for(Point2D p : points){
			if(line.relativeCCW(p) >= 0.0){
				above.add(p);
			}
		}
		
		return above;	
	}
	
	/**
	 * Recursive process to build our list
	 * @param points
	 * @return
	 */
	private List<Point2D> buildHull(List<Point2D> points, Line2D.Double line){
		//Given a baseline from point A to point B (lineAB) and a set of points above this line (pointsAB), find the point C that is farthest from the line
		//Form a line from A to C (lineAC) and select all the points to the left of this line (pointsAC)
		//Form a line from B to C (lineBC) and select all the points to the right of this line (pointsBC)

		//Base case
		if(points.size() == 1){
			finalList.add(points.get(0));			
		}else if(points.size() > 2){
			Point2D farthest = points.get(0);
			List<Point2D> doMe = new ArrayList<Point2D>();
			for(Point2D p : points){
				System.out.println(farthest.getX() + ":" + farthest.getY() + " " + p.getX() + ":" + p.getY());
				if(line.ptLineDist(p) > line.ptLineDist(farthest)){
					farthest = p;
				}
			}
			
			finalList.add(line.getP2());			
			Line2D.Double newLine = new Line2D.Double(line.getP2(), farthest);
			doMe = buildHull(getAboveLine(points, newLine), newLine);
			
//			for(Point2D p : points){
//				System.out.println(line.relativeCCW(p));
//				if(line.relativeCCW(p) >= 0.0){
//					doMe.add(p);
//				}
//			}
//			points = buildHull(doMe, new Line2D.Double(line.getP1(),farthest));
		}

//		display(points);

		return points;		
	}


	private static void display(List<Point2D> points){
		if(points != null){
			for(Point2D p : points){
				System.out.println(p.getX() + ":" + p.getY());
			}
			System.out.println();
		}
	}
}

