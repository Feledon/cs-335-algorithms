package edu.uwec.cs.muotkaht.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;

public class ConnectFourBoard implements TwoPlayerGameBoard {

	private static final int BOARD_SIZE = 42;
	private static final int ROW_LENGTH = 7;
	
	private char[] board;
	private int[] nextOpenValuesPerColumn;
	private int nextOpenPosition;
	private boolean isComputerMove;
	
	private int[][] winScenarios = {
			{0, 1, 2, 3},
			{0, 7, 14, 21},
			{0, 8, 16, 24},
			{1, 2, 3, 4},
			{1, 8, 15, 22},
			{1, 9, 17, 25},
			{2, 3, 4, 5},
			{2, 9, 16, 23},
			{2, 10, 18, 26},
			{3, 4, 5, 6},
			{3, 10, 17, 24},
			{3, 11, 19, 27},
			{4, 11, 18, 25},
			{5, 12, 19, 26},
			{6, 13, 20, 27},
			{7, 8, 9, 10},
			{7, 14, 21, 28},
			{7, 15, 23, 31},
			{8, 9, 10, 11},
			{8, 15, 22, 29},
			{8, 16, 24, 32},
			{9, 10, 11, 12},
			{9, 16, 23, 30},
			{9, 17, 25, 33},
			{10, 11, 12, 13},
			{10, 17, 24, 31},
			{10, 18, 26, 34},
			{11, 18, 25, 32},
			{12, 19, 26, 33},
			{13, 20, 27, 34},
			{14, 15, 16, 17},
			{14, 21, 28, 35},
			{14, 22, 30, 38},
			{15, 16, 17, 18},
			{15, 22, 29, 36},
			{15, 23, 31, 39},
			{16, 17, 18, 19},
			{16, 23, 30, 37},
			{16, 24, 32, 40},
			{17, 18, 19, 20},
			{17, 24, 31, 38},
			{17, 25, 33, 41},
			{18, 25, 32, 39},
			{19, 26, 33, 40},
			{20, 27, 34, 41},
			{21, 22, 23, 24},
			{21, 15, 9, 3},
			{22, 23, 24, 25},
			{22, 16, 10, 4},
			{23, 24, 25, 26},
			{23, 17, 11, 5},
			{24, 25, 26, 27},
			{24, 18, 12, 6},
			{28, 29, 30, 31},
			{28, 22, 16, 10},
			{29, 30, 31, 32},
			{29, 23, 17, 11},
			{30, 31, 32, 33},
			{30, 24, 18, 12},
			{31, 32, 33, 34},
			{31, 25, 19, 13},
			{35, 36, 37, 38},
			{35, 29, 23, 17},
			{36, 37, 38, 39},
			{36, 30, 24, 18},
			{37, 38, 39, 40},
			{37, 31, 25, 19},
			{38, 39, 40, 41},
			{38, 32, 26, 20}
	};
	
	public ConnectFourBoard() {
		this.board = new char[BOARD_SIZE];

		for (int i = 0; i < BOARD_SIZE; i++) {
			this.board[i] = '_';
		}
		
		this.nextOpenValuesPerColumn = new int[ROW_LENGTH];
		for (int i = 0; i < ROW_LENGTH; i++) {
			this.nextOpenValuesPerColumn[i] = BOARD_SIZE / ROW_LENGTH - 1;
		}
		
		this.nextOpenPosition = BOARD_SIZE - ROW_LENGTH;
		this.isComputerMove = false;
	}
	
	// Copy Constructor
	public ConnectFourBoard(ConnectFourBoard orig) {
		this.board = new char[BOARD_SIZE];
		for (int i = 0; i < BOARD_SIZE; i++) {
			this.board[i] = orig.board[i];
		}
		
		this.nextOpenValuesPerColumn = new int[ROW_LENGTH];
		for (int i = 0; i < ROW_LENGTH; i++) {
			this.nextOpenValuesPerColumn[i] = orig.nextOpenValuesPerColumn[i];
		}
		
		this.nextOpenPosition = orig.nextOpenPosition;
		this.isComputerMove = orig.isComputerMove;
	}
	
	// Checks if there are move children
	@Override
	public boolean hasMoreChildren() {
		return (nextOpenPosition < BOARD_SIZE);
	}

	// Next Child to check
	@Override
	public TwoPlayerGameBoard nextChild() {
		ConnectFourBoard child = new ConnectFourBoard(this);
		
		// computer move Red, user Blue
		child.board[this.nextOpenPosition] = (this.isComputerMove) ? 'R' : 'B';
		int nextOpenCol = this.nextOpenPosition % ROW_LENGTH;
		child.nextOpenValuesPerColumn[nextOpenCol]--;
		
		//set up to produce another child
		do {
			nextOpenCol++;
		} while (nextOpenCol < ROW_LENGTH && this.nextOpenValuesPerColumn[nextOpenCol] == -1);		
		this.nextOpenPosition = (nextOpenCol != ROW_LENGTH) ? this.nextOpenValuesPerColumn[nextOpenCol] * ROW_LENGTH + nextOpenCol : BOARD_SIZE;
		
		//set up the child
		child.isComputerMove = !child.isComputerMove;
		child.nextOpenPosition = BOARD_SIZE - ROW_LENGTH;
		findNextOpenPosition(child);
		
		return child;
	}
	
	private static int findNextOpenPosition(ConnectFourBoard state) {		
		int col = state.nextOpenPosition % ROW_LENGTH;
		
		// Column with next open spot
		while (col < ROW_LENGTH && state.nextOpenValuesPerColumn[col] == -1) {
			col++;
		}
		
		// No open spots
		if (col == ROW_LENGTH) {
			state.nextOpenPosition = BOARD_SIZE;
			
		} else { // there is open spot
			
			state.nextOpenPosition = state.nextOpenValuesPerColumn[col] * ROW_LENGTH + col;
		}
		
		return col;
	}

	// Evaluates current state
	@Override
	public double staticEvaluation() {
		boolean hasSomeoneWon = hasWon();
		if (isComputerMove && hasSomeoneWon) {
			return -1.0;
			
		} else if (!isComputerMove && hasSomeoneWon) {
			return 1.0;
			
		} else {
			return 0.0;
		}
	}

	// Draw connect 4 board
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.white);
		
		//draw the vertical lines
		for (int i = 0; i < ROW_LENGTH - 1; i++) {
			g.drawLine(50 * i + 50, 0, 50 * i + 50, 300);
		}
		
		//draw the horizontal lines
		for (int i = 0; i < 5; i++) {
			g.drawLine(0, 50 * i + 50, 350, 50 * i + 50);
		}
		
			
		//runs through every square on the board, drawing red and blue discs on the screen when necessary
		for (int i = 0; i < board.length; i++) {
			
			if (board[i] == 'R') { // Places the red 
				g.setColor(Color.RED);
				g.fillOval((i % ROW_LENGTH) * 50 + 5, (i / ROW_LENGTH) * 50 + 5, 40, 40);
				
			} else if (board[i] == 'B') { // Places the Blue
				g.setColor(Color.BLUE);
				g.fillOval((i % ROW_LENGTH) * 50 + 5, (i / ROW_LENGTH) * 50 + 5, 40, 40);
			}
		}
		
	}

	// Places move based on mouse click
	@Override
	public void placeUserMove(Point2D mouseLocation) throws Exception {
		int col = (int) mouseLocation.getX() / 50;
		
		//given the column in which the user clicked, find the spot into which the piece will drop
		int row = -1;
		int boardIndex = (row + 1) * ROW_LENGTH + col;
		while (row < 5 && this.board[boardIndex] == '_') {
			row++;
			boardIndex = (row + 1) * ROW_LENGTH + col;
		}
		boardIndex = row * ROW_LENGTH + col;
				
		//throw an exception if row or col are not between 0 and 2, or if a move has already been made at the chosen square
		if (row > 5 || col >= ROW_LENGTH || row < 0 || col < 0 || board[boardIndex] == 'B' || board[boardIndex] == 'R') {
			throw new Exception();
		}
		
		this.board[boardIndex] = 'B';
		
		this.isComputerMove = !this.isComputerMove;
		
		//indicate that this spot has been taken
		this.nextOpenValuesPerColumn[col]--;

		findNextOpenPosition(this);
	}
	
	// Checks if computer winner
	@Override
	public boolean isComputerWinner() {
		return (!this.isComputerMove && hasWon());
	}
	
	// Checks if draw
	@Override
	public boolean isDraw() {
		boolean isGameFinished = true;
		for (int i = 0; i < this.board.length && isGameFinished; i++) {
			if (this.board[i] == '_') {
				isGameFinished = false;
			}
		}
		return (isGameFinished && !hasWon());
	}

	// Checks if user winner
	@Override
	public boolean isUserWinner() {
		return (this.isComputerMove && hasWon());	
	}
	
	// Connect 4 board printout
	public String toString() {
		String boardString = "";
		
		for (int i = 0; i < BOARD_SIZE; i++) {
			boardString += board[i] + "\t";
			
			
			if ((i + 1) % ROW_LENGTH == 0) {
				boardString += "\n";
			}
		}
		return boardString;
	}
	
	// Checks if there is a winner
	private boolean hasWon() {
		boolean hasSomeoneWon = false;
		for (int scenario = 0; scenario < this.winScenarios.length && !hasSomeoneWon; scenario++) {
			
			if (this.board[this.winScenarios[scenario][0]] != '_' && 
				this.board[this.winScenarios[scenario][0]] == this.board[this.winScenarios[scenario][1]] && 
				this.board[this.winScenarios[scenario][0]] == this.board[this.winScenarios[scenario][2]] && 
				this.board[this.winScenarios[scenario][0]] == this.board[this.winScenarios[scenario][3]]) {
				
				hasSomeoneWon = true;
			}
		}
		
		return hasSomeoneWon;
	}

}

